<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');
// Version 1 of our API
$api->version('v1', function ($api) {
    $api->group([
        'namespace' => 'App\Api\Controllers',
        'middleware' => ['App\Http\Middleware\ProfileDingoHttpResponse:class'],
    ],
        function ($api) {
            $api->post('login','Auth\AuthController@login');
        });
});


/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
