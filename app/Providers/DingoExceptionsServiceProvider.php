<?php

namespace App\Providers;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class DingoExceptionsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('Dingo\Api\Exception\Handler')->register(function (ModelNotFoundException $exception) {
            $nameModel = strtolower(preg_replace('/^.*\\\/', '', $exception->getModel()));
            $errorResponse['errors'] = [
                 $nameModel => [Lang::get('validation.exists',['attribute' => Lang::get('validation.attributes.'.$nameModel)])]
            ];
            return Response::make($errorResponse, 404);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
