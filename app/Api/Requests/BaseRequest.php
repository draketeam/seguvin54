<?php

namespace App\Api\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Dingo\Api\Exception\ResourceException;
use Illuminate\Contracts\Validation\Validator;


class BaseRequest extends FormRequest
{
    protected function formatErrors(Validator $validator)
    {
        $errors = [];
        foreach ($validator->failed() as $input => $validations) {
            $currentError = $validator->errors()->get($input);
            $count = 0;
            foreach ($validations as $validation => $attribute) {
                $validation = strtolower($validation);
                $errors[$input]['message'][] = $currentError[$count];
                $errors[$input]['code'][$validation] = implode(',', $attribute);
                ++$count;
            }
        }
        return $errors;
    }

    public function response(array $errors)
    {
        throw new ResourceException('Please fix the following Errors', $errors);
    }
}