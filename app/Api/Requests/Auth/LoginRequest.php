<?php

namespace App\Api\Requests\Auth;

use App\Api\Requests\BaseRequest;
use Carbon\Carbon;

use Session;

class LoginRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
/*        if (!auth()->check()) {
            return false;
        }*/
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
           'password' => 'required|between:3,25'
        ];
    }
}