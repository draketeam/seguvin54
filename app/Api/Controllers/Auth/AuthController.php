<?php

namespace App\Api\Controllers\Auth;

use App\Api\Controllers\BaseController;
use App\Api\Requests\Auth\LoginRequest;
use Dingo\Api\Exception\ResourceException;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Support\Facades\Lang;

class AuthController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('logout');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|between:6,25|confirmed'
        ]);

        $user = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save();

        return response()
            ->json([
                'registered' => true
            ]);
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)
            ->first();

        if ($user && Hash::check($request->password, $user->password)) {
            // generate new api token
            $user->api_token = str_random(60);
            $user->save();

            return response()
                ->json([
                    'authenticated' => true,
                    'api_token' => $user->api_token,
                    'user_id' => $user->id
                ]);
        }

        throw new ResourceException('Please fix the following Errors', [
            'email' => ['message' => false, 'code' => false],
            'password' => ['message' => false, 'code' => false],
        ]);

    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->api_token = null;
        $user->save();

        return response()
            ->json([
                'done' => true
            ]);
    }
}