<ul class="sidebar-menu">
    <li class="header">Plugins</li>
    <!-- Optionally, you can add icons to the links -->
    <li  @if(Request::is('*/pusher')) class="active" @endif  ><a href="{{URL::to('admin/pusher')}}" ><i class="fa fa-link"></i> <span>Pusher.com</span></a></li>
    <li @if(isset($section) && $section === 'ajaxupload') class="active" @endif><a href="{{ URL::to('admin/ajaxuploadfile') }}"><i class="fa fa-link"></i> <span>Ajax Upload</span></a></li>

</ul>