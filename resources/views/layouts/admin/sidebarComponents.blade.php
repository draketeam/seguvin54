<ul class="sidebar-menu">
    <li class="header">Components</li>
    <!-- Optionally, you can add icons to the links -->
    <li  @if(Request::is('*/galleries')) class="active" @endif  ><a href="{{URL::to('admin/galleries')}}" ><i class="fa fa-link"></i> <span>Galleries</span></a></li>
</ul>