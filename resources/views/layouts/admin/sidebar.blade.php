<ul class="sidebar-menu">
    <li class="header">Menu</li>
    <!-- Optionally, you can add icons to the links -->

    <li class="{{$menu == 'giphy' ? "active" : "" }}"><a href="{{url("admin/giphy")}}"><i class="fa fa-link"></i> <span>Giphy</span></a></li>
    <li class="{{$menu == 'local' ? "active" : "" }}"><a href="{{url("admin/local")}}"><i class="fa fa-link"></i> <span>Local</span></a></li>
    <li class="{{$menu == 'stats' ? "active" : "" }}"><a href="{{url("admin/stats")}}"><i class="fa fa-bar-chart"></i> <span>Estadisticas</span></a></li>
{{--    <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>--}}
{{--    <li class="treeview">
        <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
        </ul>
    </li>--}}
</ul>