<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Login Language Lines
    |--------------------------------------------------------------------------
    | Login form
    |
    */
    'email' => [
        'label' => Lang::get('words.email'),
        'info' => 'Escribe el '.Lang::get('words.email').' con el que te registraste',
        'error' => Lang::get('errors.please_enter_valid',['label' => Lang::get('words.email')]),
        'success' => '',
    ],
    'password' => [
        'label' => 'Contraseña',
        'info' => '',
        'error' => '',
        'success' => '',
    ],
//        'infoEmail' => 'Type a Valid Email Address',
];
