<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Login Language Lines
    |--------------------------------------------------------------------------
    | Login form
    |
    */
    'email' => [
        'the_attribute' => Lang::get('validation.the_attribute',['attribute' => Lang::get('words.email')]),
        'label' => Lang::get('words.email'),
        'info' => 'Type the ' . Lang::get('words.email') . ' you used to register',

        'error' => [
            'required' => Lang::get('validation.filled'),
            'email' => Lang::get('validation.email'),
            'between'  => Lang::get('validation.between.string',['min' => ':0', 'max' => ':1']),
        ],
        'success' => '',
    ],
    'password' => [
        'the_attribute' => Lang::get('validation.the_attribute',['attribute' => Lang::get('words.password')]),

        'label' =>  Lang::get('words.password'),
        'info' => '',
        'error' => [
            'required' => Lang::get('validation.filled', ['attribute' => Lang::get('words.password')]),
            'between'  => Lang::get('validation.between.string',['min' => ':0', 'max' => ':1']),
            'different'  => Lang::get('validation.different',['other' => ':0']),
            'numeric'  => Lang::get('validation.numeric'),
        ],
        'success' => '',
    ],
//        'infoEmail' => 'Type a Valid Email Address',
];
