<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the messages used by
    | the app. Some of these rules are set for error, succes, information etc,
    | Feel free to tweak each of these messages here.
    |
    */

    'errors' => [
      'loginNotValid' => 'Sorry, the :email and :password you entered do not match. Please try again',
    ],
];