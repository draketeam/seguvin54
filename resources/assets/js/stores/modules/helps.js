import init, {
  actionSetStatus,
  actionValidatedDelay,
  actionValidated,
  actionSetDefaults,
  actionValidateSubmit,
  actionSetMessage,
  actionBuildStatus,
} from '../../actions/actionsHelps';

const actions = {
  init,
  actionSetStatus,
  actionValidatedDelay,
  actionValidated,
  actionSetDefaults,
  actionValidateSubmit,
  actionSetMessage,
  actionBuildStatus,
};
const getters = {};
const state = {};
const mutations = {
  SET_MESSAGE(state, params) {
    state.message = params.message;
  },
  SET_STATUS(state, params) {
    Vue.set(state, params.forInput, {
      type: params.type,
      message: params.message,
      code: params.code,
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
