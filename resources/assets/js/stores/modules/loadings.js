import init, { actionSetIsProcessing } from '../../actions/actionsLoadings';

const actions = {
  init,
  actionSetIsProcessing,
};
const getters = {};
const state = {};
const mutations = {
  SET_IS_PROCESSING(state, params) {
    if (typeof params['scope'] !== 'undefined') {
      // Validate for Empty vars
      params['isProcessing'] = params['isProcessing'] !== 'undefined' ? false : params['isProcessing'];
      state[params.scope] = { ...state[params.scope], isProcessing: params.isProcessing};
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
