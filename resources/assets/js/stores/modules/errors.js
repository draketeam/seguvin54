const actions = {};
const getters = {};
const state = {};
const mutations = {};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
