import {
  actionSetForm,
  actionLogin,
} from '../../actions/actionsAuths';


const actions = {
  actionSetForm,
  actionLogin,

};
const getters = {};
const state = {
  lang : {},
  api_token: null,
  user_id: null,
  form: {
    email: '',
    password: '',
    rememberMe: false,
  },
};
const mutations = {
  SET_LANG(state,params) {
    state.lang = params.lang;
  },
  SET_FORM(state, params) {
    switch (true) {
      case typeof params.email !== 'undefined': {
        state.form.email = params.email;
        break;
      }
      case typeof params.password !== 'undefined': {
        state.form.password = params.password;
        break;
      }
      case typeof params.rememberMe !== 'undefined': {
        state.form.rememberMe = params.rememberMe;
        break;
      }
    }
  },
  initialize() {
    this.state.api_token = localStorage.getItem('api_token');
    this.state.user_id = parseInt(localStorage.getItem('user_id'));
  },
  set(api_token, user_id) {
    localStorage.setItem('api_token', api_token);
    localStorage.setItem('user_id', user_id);
    this.initialize();
  },
  remove() {
    localStorage.removeItem('api_token');
    localStorage.removeItem('user_id');
    this.initialize();
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
