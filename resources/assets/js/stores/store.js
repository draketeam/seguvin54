import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import auths from './modules/auths';
import loadings from './modules/loadings';
import helps from './modules/helps';

Vue.use(Vuex);
Vue.config.devtools = true;
const debug = process.env.NODE_ENV !== 'production';

// Store
export default new Vuex.Store({
  modules: {
    helps,
    auths,
    loadings,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
