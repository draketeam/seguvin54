import { injectScope } from '../helpers/utils';

export default ({dispatch}, params) => {
  dispatch('actionSetIsProcessing', params);
};
export const actionSetIsProcessing = ({commit, getters}, params) => {
  params = injectScope(params, getters.currentRoute);
  commit('SET_IS_PROCESSING', params);
};
