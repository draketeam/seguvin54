export default ({dispatch}, params) => {
  Object.keys(params).forEach((inputs) => {
    dispatch('actionSetDefaults', inputs);
  });
};
const touchMap = new WeakMap();

export const actionSetDefaults = ({dispatch}, inputs) => {
  if (window.app.$i18n.te(`${inputs}.info`)) {
    dispatch('actionSetStatus', {
      forInput: inputs,
      type: 'info',
      message: '',
      code: 'info',
    });
  }
};

export const actionSetStatus = ({commit}, params) => {
  commit('SET_STATUS', params);
};

export const actionSetMessage = ({commit}, params) => {
  if (typeof params.message !== 'undefined') {
    commit('SET_MESSAGE', {
      message: params.message,
    });
  }
};
export const actionBuildStatus = ({dispatch}, params) => {
  if (typeof params.name !== 'undefined' && typeof params.type !== 'undefined' && typeof params.message !== 'undefined' && typeof params.code !== 'undefined') {
    dispatch('actionSetStatus', {
      forInput: params.name,
      type: params.type,
      message: params.message,
      code: params.code,
    });
  }
};

export const actionValidatedDelay = ({dispatch}, params) => {
  if (typeof params.name !== 'undefined' && typeof params.validate !== 'undefined') {
    var $v = params.validate;
    $v.$reset();
    if (touchMap.has($v)) {
      clearTimeout(touchMap.get($v));
    }
    touchMap.set($v, setTimeout(() => { dispatch('actionValidated', params); }, 500));
  }
};

export const actionValidated = ({dispatch}, params) => {
  if (typeof params.name !== 'undefined' && typeof params.validate !== 'undefined') {
    params.validate.$touch();
    if (params.validate.$invalid === false) {
      dispatch('actionSetStatus', {
        forInput: params.name,
        type: 'success',
        message: '',
        code: 'success',
      });
      return true;
    }
    Object.keys(params.validate.$params).forEach((validation) => {
      if (params.validate[validation] === false) {
        dispatch('actionSetStatus', {
          forInput: params.name,
          type: 'error',
          message: '',
          code: `${params.name}.error.${validation}`,
        });
      }
    });
  }
};

export const actionValidateSubmit = ({dispatch}, validate) => {
  if (validate.$invalid === true) {
    Object.keys(validate.$params).forEach((inputs) => {
      dispatch('actionValidated', {name: inputs, validate: validate[inputs]});
    });
  }
};