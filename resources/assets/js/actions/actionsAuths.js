import { get, post } from '../helpers/api';
import { injectScope } from '../helpers/utils';


export const actionSetForm = ({commit}, params) => {
  commit('SET_FORM', params);
};

export const actionLogin = ({commit, dispatch, getters }, params) => {
  post('api/login', params.form)
    .then((response) => {
      console.log(response.data);
    })
    .catch((err) => {
      if (err.response.status === 422) {
        // TODO-fer Show global errors on a flash screen
        // dispatch('helps/actionSetMessage', {message: err.response.data.message}, {root: true});
        _.each(err.response.data.errors, (error, key) => {
          dispatch('helps/actionBuildStatus', {name: key, type: 'error', code: error.code, message: error.message}, {root: true});
        });
      }
    });
  /*
   this.isProcessing = true;
   this.error = {};
   post('api/login', this.form)
   .then((res) => {
   if (res.data.authenticated) {
   // set token
   Auth.set(res.data.api_token, res.data.user_id);
   Flash.setSuccess('You have successfully logged in.');
   this.$router.push('/');
   }
   this.isProcessing = false;
   })
   .catch((err) => {
   if (err.response.status === 422) {
   this.error = err.response.data;
   }
   this.isProcessing = false;
   });*/
  // commit('SET_FORM', params.form);
};
