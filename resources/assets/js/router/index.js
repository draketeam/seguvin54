import Vue from 'vue';
import VueRouter from 'vue-router';
// Router Links
// import Login from '../views/Auth/Login.vue';

 const Login = resolve => {
 require.ensure([], require => resolve(require('../views/Auth/Login.vue')), 'login');
 };
Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [
    {path: '/login', name: 'Login', component: Login},
    {path: '/register', name: 'Register', component: Login},
  ],
});
