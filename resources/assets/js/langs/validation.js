export default {
    "en": {
        "the_attribute": "The {attribute}",
        "accepted": " must be accepted",
        "active_url": " is not a valid URL",
        "after": " must be a date after {date}",
        "after_or_equal": " must be a date after or equal to {date}",
        "alpha": " may only contain letters",
        "alpha_dash": " may only contain letters, numbers, and dashes",
        "alpha_num": " may only contain letters and numbers",
        "array": " must be an array",
        "before": " must be a date before {date}",
        "before_or_equal": " must be a date before or equal to {date}",
        "between": {
            "numeric": " must be between {min} and {max}",
            "file": " must be between {min} and {max} kilobytes",
            "string": " must be between {min} and {max} characters",
            "array": " must have between {min} and {max} items"
        },
        "boolean": " field must be true or false",
        "confirmed": " confirmation does not match",
        "date": " is not a valid date",
        "date_format": " does not match the format {format}",
        "different": " and {other} must be different",
        "digits": " must be {digits} digits",
        "digits_between": " must be between {min} and {max} digits",
        "dimensions": " has invalid image dimensions",
        "distinct": " field has a duplicate value",
        "email": " must be a valid email address",
        "exists": "The selected {attribute} is invalid",
        "file": " must be a file",
        "filled": " field must have a value",
        "image": " must be an image",
        "in": "The selected {attribute} is invalid",
        "in_array": " field does not exist in {other}",
        "integer": " must be an integer",
        "ip": " must be a valid IP address",
        "ipv4": " must be a valid IPv4 address",
        "ipv6": " must be a valid IPv6 address",
        "json": " must be a valid JSON string",
        "max": {
            "numeric": " may not be greater than {max}",
            "file": " may not be greater than {max} kilobytes",
            "string": " may not be greater than {max} characters",
            "array": " may not have more than {max} items"
        },
        "mimes": " must be a file of type: {values}",
        "mimetypes": " must be a file of type: {values}",
        "min": {
            "numeric": " must be at least {min}",
            "file": " must be at least {min} kilobytes",
            "string": " must be at least {min} characters",
            "array": " must have at least {min} items"
        },
        "not_in": "The selected {attribute} is invalid",
        "numeric": " must be a number",
        "present": " field must be present",
        "regex": " format is invalid",
        "required": " field is required",
        "required_if": " field is required when {other} is {value}",
        "required_unless": " field is required unless {other} is in {values}",
        "required_with": " field is required when {values} is present",
        "required_with_all": " field is required when {values} is present",
        "required_without": " field is required when {values} is not present",
        "required_without_all": " field is required when none of {values} are present",
        "same": " and {other} must match",
        "size": {
            "numeric": " must be {size}",
            "file": " must be {size} kilobytes",
            "string": " must be {size} characters",
            "array": " must contain {size} items"
        },
        "string": " must be a string",
        "timezone": " must be a valid zone",
        "unique": " has already been taken",
        "uploaded": " failed to upload",
        "url": " format is invalid",
        "custom": {
            "attribute-name": {
                "rule-name": "custom-message"
            }
        },
        "attributes": []
    }
}
