export default {
    "en": {
        "errors": {
            "loginNotValid": "Sorry, the {email} and {password} you entered do not match. Please try again"
        }
    }
}
