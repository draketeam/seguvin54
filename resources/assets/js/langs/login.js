export default {
    "en": {
        "email": {
            "the_attribute": "The Email Address",
            "label": "Email Address",
            "info": "Type the Email Address you used to register",
            "error": {
                "required": " field must have a value",
                "email": " must be a valid email address",
                "between": " must be between {0} and {1} characters"
            },
            "success": ""
        },
        "password": {
            "the_attribute": "The Password",
            "label": "Password",
            "info": "",
            "error": {
                "required": " field must have a value",
                "between": " must be between {0} and {1} characters",
                "different": " and {0} must be different",
                "numeric": " must be a number"
            },
            "success": ""
        }
    },
    "es": {
        "email": {
            "label": "Correo Electronico",
            "info": "Escribe el Correo Electronico con el que te registraste",
            "error": "Porfavor escribe Correo Electronico valido",
            "success": ""
        },
        "password": {
            "label": "Contraseña",
            "info": "",
            "error": "",
            "success": ""
        }
    }
}
