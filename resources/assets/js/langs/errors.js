export default {
    "en": {
        "please_enter_valid": "Please enter a valid {label}",
        "only_email": "You can Only insert Email Address"
    },
    "es": {
        "please_enter_valid": "Porfavor escribe {label} valido",
        "only_email": "You can Only insert Email Address"
    }
}
