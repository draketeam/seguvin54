export const currentRoute = (state, getters, rootState) =>{
  return rootState.route.name.toLowerCase();
};

export const currentModule = (state, getters) => {
  return state[getters.currentRoute];
};