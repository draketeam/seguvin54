import axios from 'axios';

export const get = (url, token) => axios({
    method: 'GET',
    url: url,
    header: {
      'Authorization': `Bearer ${token}`,
    },
  });

export const post = (url, payload, token) => axios({
    method: 'POST',
    url: url,
    data: payload,
    header: {
      'Authorization': `Bearer ${token}`,
    },
  });
export const del = (url, token) => axios({
    method: 'DELETE',
    url: url,
    header: {
      'Authorization': `Bearer ${token}`,
    },
  });

export const interceptors = (cb) => {
  axios.interceptors.response.use(res => res, (err) => {
    cb(err);
    return Promise.reject(err);
  });
};
