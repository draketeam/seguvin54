export const injectScope = (params, scope) => {
  if (typeof params === 'undefined') {
    return {scope: scope};
  }
  if (typeof params['scope'] === 'undefined') {
    params['scope'] = scope;
    return params;
  }
  return params;
};
