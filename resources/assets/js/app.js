import { sync } from 'vuex-router-sync';
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import App from './app.vue';
import store from './stores/store';
import router from './router/index';


window.Vue = Vue;
window._ = require('lodash');
require('admin-lte');
require('bootstrap');


/*
 window.axios = require('axios');

 window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

 let token = document.head.querySelector('meta[name="csrf-token"]');

 if (token) {
 window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
 } else {
 console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
 }*/
// Ready translated locale messages

Vue.use(VueI18n);

sync(store, router);

const i18n = new VueI18n({
  locale: 'en',
});

/* eslint-disable no-new */
const app = window.app =  new Vue({
  el: '#root',
  i18n,
  store,
  router,
  components: {App},
});
