export default {
  methods: {
    initLanguages(locales) {
      Object.keys(locales).forEach((lang) => {
        this.$i18n.setLocaleMessage(lang, locales[lang]);
      });
    },
  },
};
