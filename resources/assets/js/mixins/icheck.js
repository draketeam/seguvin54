window.$ = window.jQuery = require('jquery');
require('icheck');
export default {
  methods: {
    /**
     * Jquery iCheck integration you need to set the Id or Class for the selector var
     * it will change radios and checkbox
     * and vModel is Vuex v-model will be using to change,
     * remember it need to have get and set properties
     * @param selector String
     * @param vuexVModel String
     */
    iCheckMix(selector, vModel) {
      var _self = this;
      jQuery(selector + ' input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
      jQuery(selector + ' input').on('ifToggled', function (e) {
          var value = _.get(_self, vModel) === true ? '' : true;
          _.set(_self, vModel, value);
      });
    },
  },
};