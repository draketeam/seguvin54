let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
  // .extract(['vue', 'vue-i18n', 'jquery', 'lodash', 'bootstrap', 'admin-lte'])
  .autoload({
    jquery: ['$', 'jQuery', 'jquery'],
  })
  .styles([
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/icheck/skins/square/blue.css',
    'node_modules/ionicons/dist/css/ionicons.css',
    'node_modules/font-awesome/css/font-awesome.css',
    'node_modules/admin-lte/dist/css/AdminLTE.css',
    'node_modules/admin-lte/dist/css/skins/_all-skins.css',
  ], 'public/css/AdminLte.css');
//  .version();

mix.copy(['node_modules/icheck/skins/square/blue.png', 'node_modules/icheck/skins/square/blue@2x.png'], 'public/css');
mix.copy('node_modules/ionicons/dist/css/ionicons.css.map', 'public/css');
mix.copyDirectory('node_modules/bootstrap/fonts', 'public/fonts');
mix.copyDirectory('node_modules/ionicons/dist/fonts', 'public/fonts');
mix.copyDirectory('node_modules/font-awesome/fonts', 'public/fonts');
mix.copyDirectory('node_modules/admin-lte/dist/img', 'public/img/admin');
//
// mix.browserSync({
//   proxy: 'btlstatus.app',
//   files: [
//     '!app/**/*.php',
//     'resources/views/!**!/!*.php',
//     'public/js/!**!/!*.js',
//     'public/css/!**/!*.css',
//   ],
// });
